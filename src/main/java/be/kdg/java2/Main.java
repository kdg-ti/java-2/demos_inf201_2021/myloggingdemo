package be.kdg.java2;


import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        logger.info("Application started...");
        Student student = new Student("Jos");
        student.setName(null);
        student.getName();
    }
}
