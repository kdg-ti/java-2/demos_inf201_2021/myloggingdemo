package be.kdg.java2;

import java.util.logging.Logger;

public class Student {
    private static final Logger logger = Logger.getLogger(Student.class.getName());

    private String name;

    public Student(String name) {
        logger.info("Creating student with name: " + name);
        this.name = name;
    }

    public String getName() {
        logger.fine("getting the name...");
        return name;
    }

    public void setName(String name) {
        if (name==null) {
            logger.warning("Setting name to null!");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
